import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(public http: HttpClient) {

  }
  getUser():Observable<any>{
    let params = window.location.hash.split('&');
    if (params.length > 0 && params[0].startsWith('#access_token=')) {
      let key = decodeURIComponent(params[0].replace('#access_token=', ''));
      localStorage.setItem('token', key);
     return  this.http.get('https://bitbucket.org/api/2.0/user', {
        params: {
          access_token: key
        }
      })
    }

  }
  getRepositoriesWork():Observable<any>{
  
      
     return  this.http.get('https://bitbucket.org/api/2.0/user/permissions/repositories', {
        params: {
          access_token: localStorage.getItem('token')
        }
      })
    

  }

}
