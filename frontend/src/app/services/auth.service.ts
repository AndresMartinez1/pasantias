import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Location } from '@angular/common';
import {  Router,Route, NavigationStart, 
  Event as NavigationEvent, 
  NavigationCancel,
  RoutesRecognized,
  CanActivate,CanActivateChild,CanLoad,
  ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { tokenKey } from '@angular/core/src/view';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AuthService implements CanActivate, CanActivateChild, CanLoad {

  authToken: any;
  user: any;
  readonly URL_API = 'http://localhost:3000/api/profile';
  public code:string;
  public authServerBaseUrl = 'https://localhost:4200';
  public configObj = {"authEndpoint":"","clientId":"","redirectURI":""};
  public cachedURL:string;
  public loginProvider: string;
  public loading: boolean;
  public loginURI: string;
  public token:string;
  public url;
  constructor(public http:HttpClient,  private location: Location,private router:Router) {
    console.log(this.router.url, 'HOLA ANDRES YA');
    let config = {
      "loginRoute":"login", 
       "authEndpoint":"http://localhost:3000/auth",
       "clientId":"zDq7udXcDaGCGBxS3m",
       "redirectURI" : this.authServerBaseUrl+"/dashboard"
     
    };
    let provider = localStorage.getItem("provider");
    let cachedURL = localStorage.getItem('cachedurl');
    let params = new URLSearchParams(this.location.path(false).split('?')[1]);
    this.url = this.router.parseUrl(this.router.url);
    this.code = params.get('code');  
    console.log(this.code, 'HOLA ANDRES YA');

    if(config){
      this.configObj = {
         "authEndpoint": "http://localhost:3000/auth/bitbucket",
         "clientId":"zDq7udXcDaGCGBxS3m",
         "redirectURI" : this.authServerBaseUrl+"/dashboard"
       
      };
      this.loginURI =  "login";
    }
    if(this.code){
      this.login(this.code,this.configObj.clientId,this.configObj.redirectURI, this.configObj.authEndpoint)
      .then((data:any) => {
          this.loading = false;
          this.router.navigate([this.cachedURL]);
              return true;
          });
    }
  }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    console.log('ENTRO AL CAN ACTIVATE');
    let url: string = state.url;  
    return this.verifyLogin(url);
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    console.log('ENTRO AL CANCHILD');

    return this.canActivate(route, state);
  }

  canLoad(route: Route): boolean {
    console.log('ENTRO AL CANLOAD');
    let url = `/${route.path}`;

    return this.verifyLogin(url);
  }
  public auth():void{
    const id ='zDq7udXcDaGCGBxS3m';
 
    window.location.href = 'https://bitbucket.org/site/oauth2/authorize?client_id='+id+'&response_type=token';

  }
  login(code:any,clientId:any,redirectURI:any,authEndpoint:any):Promise<any>{
    var body = {"code" : code,"clientId" : clientId,"redirectUri":redirectURI}
    console.log('ENTRO AL LOGIN', body,authEndpoint);

    return this.http.post(authEndpoint,body,{})
    .toPromise()
     .then((r: Response) => { 
              localStorage.setItem('isLoggedIn', "true");
              localStorage.setItem('token',"SI");
              console.log(r.json());
              return r.json()
      })
      .catch();
      ;
   // return Observable.of(true).delay(1000).do(val => this.isLoggedIn = localStorage.getItem('isLoggedIn'));
  }
  verifyLogin(url):boolean{
    console.log('ENTRO AL Verify');

    if(!this.isLoggedIn() && this.code == null){
      console.log('ENTRO AQUI ANDRES');
      localStorage.setItem('cachedurl',url);
      this.router.navigate([this.loginURI]);
      return false;
    }
    else if(this.isLoggedIn()){
      return true;
    }
    else if(!this.isLoggedIn()  && this.code != null){
       let params = new URLSearchParams(this.location.path(false).split('?')[1]);
       if(params.get('code') && (localStorage.getItem('cachedurl') == "" || localStorage.getItem('cachedurl') == undefined)){
          localStorage.setItem('cachedurl',this.location.path(false).split('?')[0]);
       }    
       if(this.cachedURL != null || this.cachedURL != ""){
          this.cachedURL = localStorage.getItem('cachedurl');
       }
    }
  }
  public isLoggedIn(): boolean{
    let status = false;
    if( localStorage.getItem('isLoggedIn') == "true"){
      status = true;
    }
    else{
      status = false;
    }
    return status;
  }

  registerUser(user){
    let headers = new HttpHeaders();
    headers.append('Content-Type','application/json');
    return this.http.post('http://localhost:3000/users/registrer', user,{headers: headers});
     
  }

  authenticateUser(user){
    let headers = new HttpHeaders();
    headers.append('Content-Type','application/json');
    return this.http.post('http://localhost:3000/users/authenticate', user,{headers: headers});
  }

  getProfile() {
    let headers = new HttpHeaders();
    this.loadToken();
    headers=headers.append('Authorization', localStorage.getItem('id_token').toString());
    headers=headers.append('Content-Type', 'application/json');
    console.log(headers);
    return this.http.get('http://localhost:3000/users/profile', {headers: headers});
  }

  loadToken() {
    const token = localStorage.getItem('id_token');
    this.authToken = token;
  }

  storeUserData(token, user){
    localStorage.setItem('id_token', token);
    localStorage.setItem('user', JSON.stringify(user));
    this.authToken = token;
    this.user = user;
  }

  logout(){
    this.authToken = null;
    this.user = null;
    localStorage.clear();
  }

}
