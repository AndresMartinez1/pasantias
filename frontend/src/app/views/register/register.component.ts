import { Component } from '@angular/core';
import {ValidateService} from '../../services/validate.service'
import {FlashMessagesService} from 'angular2-flash-messages';
import {AuthService} from '../../services/auth.service'
import {Router} from '@angular/router';


@Component({
  selector: 'app-dashboard',
  templateUrl: 'register.component.html'
})
export class RegisterComponent {
  name:string;
  email:string;
  username:string;
  password:string;
  constructor(private router: Router,
    private validateService: ValidateService,private authService: AuthService, private flashMessage:FlashMessagesService) { }
  Register(){
    const user={
      name:this.name,
      email:this.email,
      username:this.username,
      password:this.password
    }
    if(!this.validateService.validateRegister(user)){
      this.flashMessage.show('Por favor ingresar todos los campos', {cssClass: 'alert-danger', timeout: 3000});
      return false;
    }

    // Validate Email
    if(!this.validateService.validateEmail(user.email)){
      this.flashMessage.show('Por favor ingresar un correo valido', {cssClass: 'alert-danger', timeout: 3000});
      return false;
    }

    this.authService.registerUser(user).subscribe(data => {
      if(data){
        this.flashMessage.show('You are now registered and can log in', {cssClass: 'alert-success', timeout: 3000});
        this.router.navigate(['/login']);
      } else {
        this.flashMessage.show('Something went wrong', {cssClass: 'alert-danger', timeout: 3000});
        this.router.navigate(['/register']);
      }
    });

  }

  
  
}
