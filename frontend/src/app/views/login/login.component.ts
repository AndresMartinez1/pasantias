import {AuthService} from '../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {FlashMessagesService} from 'angular2-flash-messages';


@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent { 
  username: String;
  password: String;
 constructor(public authService:AuthService, private router:Router,
  private flashMessage:FlashMessagesService){}
 bitbucketLogin(){ 
   this.authService.auth();
 }

 login(){
  const user = {
    username: this.username,
    password: this.password
  }
console.log(user);
   this.authService.authenticateUser(user).subscribe(data => {
    if(data['success']){
      console.log(data, 'Dat0s')
      this.authService.storeUserData(data['token'], data['user']);
      this.flashMessage.show('You are now logged in', {
        cssClass: 'alert-success',
        timeout: 5000});
      this.router.navigate(['dashboard']);
      console.log(localStorage);
    } else {
      console.log('NADA');
      this.flashMessage.show(data['msg'], {
        cssClass: 'alert-danger',
        timeout: 5000});
      this.router.navigate(['login']);
    }
  })
}
}
