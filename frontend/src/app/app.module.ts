import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'; 
//import { axios }  from 'axios';
//import { AuthService }      from './services/auth.service';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { FormsModule } from '@angular/forms';
import {ValidateService} from './services/validate.service';
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};
import {RouterModule, Routes} from '@angular/router';

import { AppComponent } from './app.component';
import { FlashMessagesModule } from 'angular2-flash-messages';

// Import containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';
import {AuthService} from './services/auth.service';

const APP_CONTAINERS = [
  DefaultLayoutComponent
];

import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule,
} from '@coreui/angular';

// Import routing module
import { AppRoutingModule } from './app.routing';


// Import 3rd party components
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import Axios from 'axios';
import { ProgramadoresComponent } from './views/programadores/programadores/programadores.component';
import { ProyectosComponent } from './views/proyectos/proyectos/proyectos.component';
import { EquiposComponent } from './views/equipos/equipos/equipos.component';
import { RepositoriosComponent } from './views/repositorios/repositorios/repositorios.component';
import { HistoricoComponent } from './views/historico/historico/historico.component';
import { DbComponent } from './views/db/db/db.component';
import { PerfilComponent } from './views/perfil/perfil/perfil.component';

@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    AppAsideModule,
    AppBreadcrumbModule.forRoot(),
    AppFooterModule,
    AppHeaderModule,
    AppSidebarModule,
    PerfectScrollbarModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ChartsModule,
    HttpClientModule,
    FormsModule,
    FlashMessagesModule.forRoot(),

    
  ],
  declarations: [
    AppComponent,
    ...APP_CONTAINERS,
    P404Component,
    P500Component,
    LoginComponent,
    RegisterComponent,
    ProgramadoresComponent,
    ProyectosComponent,
    EquiposComponent,
    RepositoriosComponent,
    HistoricoComponent,
    DbComponent,
    PerfilComponent,
  //  AuthService
  ],
  providers:[ValidateService,AuthService],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
