import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';
import {HashLocationStrategy, Location, LocationStrategy} from '@angular/common';
import { ProgramadoresComponent } from './views/programadores/programadores/programadores.component';
import { ProyectosComponent } from './views/proyectos/proyectos/proyectos.component';
import { EquiposComponent } from './views/equipos/equipos/equipos.component';
import { RepositoriosComponent } from './views/repositorios/repositorios/repositorios.component';
import { HistoricoComponent } from './views/historico/historico/historico.component';
import { DbComponent } from './views/db/db/db.component';
import { PerfilComponent } from './views/perfil/perfil/perfil.component';
export const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },
  {
    path: 'register',
    component: RegisterComponent,
    data: {
      title: 'Register Page'
    }
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'Inicio'
    },
    children: [
      {
        path: 'base',
        loadChildren: './views/base/base.module#BaseModule'
      },
      {
        path: 'buttons',
        loadChildren: './views/buttons/buttons.module#ButtonsModule'
      },
      {
        path: 'charts',
        loadChildren: './views/chartjs/chartjs.module#ChartJSModule'
      },
      {
        path: 'dashboard',
        loadChildren: './views/dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'icons',
        loadChildren: './views/icons/icons.module#IconsModule'
      },
      {
        path: 'notifications',
        loadChildren: './views/notifications/notifications.module#NotificationsModule'
      },
      {
        path: 'theme',
        loadChildren: './views/theme/theme.module#ThemeModule'
      },
      {
        path: 'widgets',
        loadChildren: './views/widgets/widgets.module#WidgetsModule'
      },
      {
        path: 'programadores',
        component:ProgramadoresComponent
      },
      {
        path: 'proyectos',
        component:ProyectosComponent
      },
      {
        path: 'repositorios',
        component:ProyectosComponent
      },
      {
        path: 'db',
        component:DbComponent
      },{
        path: 'perfil',
        component:PerfilComponent
      },{
        path: 'historico',
        component:HistoricoComponent
      },{
        path: 'equipos',
        component:EquiposComponent
      }
    ]
  },
  { path: '**', component: P404Component }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
