import { Component, OnDestroy, Inject, OnInit } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { navItems } from '../../_nav';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {FlashMessagesService} from 'angular2-flash-messages';
import {UserService} from '../../services/user.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent implements OnDestroy,OnInit {
  public navItems = navItems;
  public sidebarMinimized = true;
  private changes: MutationObserver;
  public element: HTMLElement;
  public User;
  public repositories:Observable<any>;
  constructor(public userService:UserService, private flashMessage:FlashMessagesService, public authService:AuthService, private router:Router, @Inject(DOCUMENT) _document?: any) {
this.userService.getUser().subscribe(res=>{
  this.User=res;
  console.log('ENtro a Default',this.User)

});

    this.changes = new MutationObserver((mutations) => {
      this.sidebarMinimized = _document.body.classList.contains('sidebar-minimized');
    });
    this.element = _document.body;
    this.changes.observe(<Element>this.element, {
      attributes: true,
      attributeFilter: ['class']
    });
  }
  ngOnInit(){

  }
  ngOnDestroy(): void {
    this.changes.disconnect();
  }

  GetRepositoriesWork(){
    this.userService.getRepositoriesWork().subscribe(res=>{this.repositories=res});

  }
  Logout(){
    this.authService.logout();
    this.flashMessage.show('You are logged out', {
      cssClass:'alert-success',
      timeout: 3000
    });
    console.log(localStorage);
    this.router.navigate(['/login']);
    return false;
  }
}
