interface NavAttributes {
  [propName: string]: any;
}
interface NavWrapper {
  attributes: NavAttributes;
  element: string;
}
interface NavBadge {
  text: string;
  variant: string;
}
interface NavLabel {
  class?: string;
  variant: string;
}

export interface NavData {
  name?: string;
  url?: string;
  icon?: string;
  badge?: NavBadge;
  title?: boolean;
  children?: NavData[];
  variant?: string;
  attributes?: NavAttributes;
  divider?: boolean;
  class?: string;
  label?: NavLabel;
  wrapper?: NavWrapper;
}

export const navItems: NavData[] = [
  {
    name: 'Panel',
    url: '/dashboard',
    icon: 'icon-speedometer',
   
  },
 
  {
    name: 'Programadores',
    url: '/programadores',
    icon: 'icon-user'
  },
  {
    name: 'Equipos',
    url: '/equipos',
    icon: 'icon-people'
  },
  {
    name: 'Proyectos',
    url: '/proyectos',
    icon: 'icon-folder-alt'
  },
  
  {
    name: 'Repositorios',
    url: '/repositorios',
    icon: 'icon-drawer',
  },
  {
    name: 'Historico',
    url: '/historico',
    icon: 'icon-list'
  },
  {
    name: 'Manual',
    url: '/theme/colors',
    icon: 'icon-book-open'
  },
  {
    name: 'Base de datos',
    url: '/db',
    icon: 'icon-cloud-upload',
    
  },
  {
    name: 'Información',
    url: '/charts',
    icon: 'icon-info'
  }

];
