const express=require('express');
const router =express.Router();
const authCtrl=require('../Controllers/auth.controller');


router.post('/auth/bitbucket', (req, res)=> {
   
    var accessTokenUrl = 'https://bitbucket.org/site/oauth2/access_token';
    var userApiUrl = 'https://bitbucket.org/api/2.0/user';
    var emailApiUrl = 'https://bitbucket.org/api/2.0/user/emails';
    var clientId='zDq7udXcDaGCGBxS3m';
    var secret='Ku2DMwqdz36CN65w9GVMqUbgfLDHFfUM';


    var headers = {
      Authorization: 'Basic ekRxN3VkWGNEYUdDR0J4UzNtOkt1MkRNd3FkejM2Q042NXc5R1ZNcVViZ2ZMREhGZlVN'
    };
  
    var formData = {
        code:req.body.code,    
    redirect_uri: 'http:/localhost:4200/dashboard',
      grant_type: 'authorization_code'
    };
  
    // Step 1. Exchange authorization code for access token.
    require.post({ url: accessTokenUrl, form: formData, headers: headers, json: true }, function(err, response, body) {
      if (body.error) {
        return res.status(400).send({ message: body.error_description });
      }
  
      var params = {
        access_token: body.access_token
      };
  console.log()
      
      // Step 2. Retrieve information about the current user.
      req.get({ url: userApiUrl, qs: params, json: true }, function(err, response, profile) {
  
        // Step 2.5. Retrieve current user's email.
        request.get({ url: emailApiUrl, qs: params, json: true }, function(err, response, emails) {
          var email = emails.values[0].email;
  
          // Step 3a. Link user accounts.
          if (req.header('Authorization')) {
            User.findOne({ bitbucket: profile.uuid }, function(err, existingUser) {
              if (existingUser) {
                return res.status(409).send({ message: 'There is already a Bitbucket account that belongs to you' });
              }
              var token = req.header('Authorization').split(' ')[1];
              var payload = jwt.decode(token, secret);
              User.findById(payload.sub, function(err, user) {
                if (!user) {
                  return res.status(400).send({ message: 'User not found' });
                }
                user.bitbucket = profile.uuid;
                user.email = user.email || email;
                user.picture = user.picture || profile.links.avatar.href;
                user.displayName = user.displayName || profile.display_name;
                user.save(function() {
                  var token = createJWT(user);
                  res.send({ token: token });
                });
              });
            });
          } else {
            // Step 3b. Create a new user account or return an existing one.
            User.findOne({ bitbucket: profile.id }, function(err, existingUser) {
              if (existingUser) {
                var token = createJWT(existingUser);
                return res.send({ token: token });
              }
              var user = new User();
              user.bitbucket = profile.uuid;
              user.email = email;
              user.picture = profile.links.avatar.href;
              user.displayName = profile.display_name;
              user.save(function() {
                var token = createJWT(user);
                res.send({ token: token });
              });
            });
          }
        });
      });
    });
  });

  router.post('/bitbucket',(req,res)=>{
    
    req.post('https://bitbucket.org/site/oauth2/access_token',)
  })
module.exports=router;