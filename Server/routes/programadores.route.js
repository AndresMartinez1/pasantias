const express=require('express');
const router =express.Router();
const programadorCtrl=require('../Controllers/programador.controller');


router.get('/', programadorCtrl.getProgramadores);
router.post('/',programadorCtrl.createProgramador);
router.get('/:id', programadorCtrl.getProgramador);
router.put('/:id', programadorCtrl.editProgramador);
router.delete('/:id', programadorCtrl.deleteProgramador);

module.exports=router;