const Programador = require('../models/programadores');

const programadorCtrl={};

programadorCtrl.getProgramadores=
    async (req,res)=>{
      const programadores= await Programador.find();
      res.json(programadores);
      
    }


programadorCtrl.createProgramador= async (req,res)=>{
   const progra = new Programador(req.body);
    await progra.save();
    res.json({
        mensaje:'recibido'
    });

    
}

programadorCtrl.getProgramador= async(req,res)=>{
    const programador=await Programador.findById(req.params.id);
    console.log(programador);
    res.json(programador
    );

}

programadorCtrl.editProgramador=async(req,res)=>{
const {id}=req.params;
    const progra={
    name:req.body.name,
    email:req.body.email,
    country:req.body.country
};
await Programador.findByIdAndUpdate(id, {$set:progra},{new:true});
res.json(progra);
}

programadorCtrl.deleteProgramador=async (req,res)=>{
    const {id}=req.params;

await Programador.findByIdAndDelete(id);
res.json({status:"eliminado"});
};


module.exports=programadorCtrl;