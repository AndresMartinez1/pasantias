const express=require('express');
const morgan=require('morgan');
const app=express();
const  passport = require('passport'),
  jwt = require('jsonwebtoken'),
  expressJwt = require('express-jwt'),
  router = express.Router(),
  cors = require('cors'),
  bodyParser = require('body-parser');
  const path = require('path');
  
  var request = require('request');


  app.use(passport.initialize());
  app.use(passport.session());
  
 
//const jwt=require('jsonwebtoken');
const {mongoose} =require('./database');
//Settings
app.set('port',process.env.port || 3000);
//Middlewares
app.use(morgan('dev'));
app.use(express.json());

//Routes
app.use('/api/programadores', require('./routes/programadores.route'));
//app.use('/auth',require('./routes/auth.route'));
app.use('/',require('./routes/auth.route'));

//mongoose file must be loaded before all other files in order to provide
// models to other modules
app.post('/',(req,res)=>{
  res.send('INICIO')

});

var User = require('mongoose').model('User');

//setup configuration for facebook login


// enable cors
var corsOption = {
  origin: true,
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  credentials: true,
  exposedHeaders: ['x-auth-token']
};
app.use(cors(corsOption));

//rest API requirements

app.use(bodyParser.json());

app.use(passport.initialize());
app.use(passport.session());

router.route('/health-check').get(function(req, res) {
  res.status(200);
  res.send('Hello World');
});

var createToken = function(auth) {
  return jwt.sign({
    id: auth.id
  }, 'Ku2DMwqdz36CN65w9GVMqUbgfLDHFfUM',
  {
    expiresIn: 60 * 120
  });
};
app.use(express.static(path.join(__dirname,'public')));



var generateToken = function (req, res, next) {
  req.token = createToken(req.auth);
  return next();
};
app.use('/users',require('./routes/users.route') );
var sendToken = function (req, res) {
  res.setHeader('x-auth-token', req.token);
  return res.status(200).send(JSON.stringify(req.user));
};
app.get('/auth/bitbucket',
passport.authenticate('bitbucket'));

router.route('/auth/bitbucket/callback')
  .post(passport.authenticate('bitbucket', {session: false}), function(req, res, next) {
    if (!req.user) {
      return res.send(401, 'User Not Authenticated');
    }

    // prepare token for API
    req.auth = {
      id: req.user.id
    };

    return next();
  }, generateToken, sendToken);

//token handling middleware
var authenticate = expressJwt({
  secret: 'Ku2DMwqdz36CN65w9GVMqUbgfLDHFfUM',
  requestProperty: 'auth',
  getToken: function(req) {
    if (req.headers['x-auth-token']) {
      console.log('MAlPARIDO');
      return req.headers['x-auth-token'];
    }
    return null;
  }
});

var getCurrentUser = function(req, res, next) {
  User.findById(req.auth.id, function(err, user) {
    if (err) {
      next(err);
    } else {
      req.user = user;
      next();
    }
  });
};

var getOne = function (req, res) {
  var user = req.user.toObject();

  delete user['bitbucketProvider'];
  delete user['__v'];

  res.json(user);
};

router.route('/auth/me')
  .get(authenticate, getCurrentUser, getOne);

app.use('/', router);


require('./passport')(passport);


app.post('/prueba', function(req, res) {
  var accessTokenUrl = 'https://bitbucket.org/site/oauth2/access_token';
  var userApiUrl = 'https://bitbucket.org/api/2.0/user';
  var emailApiUrl = 'https://bitbucket.org/api/2.0/user/emails';
  console.log('LLEGO AQUI');
  var headers = {
    Authorization: 'Basic ekRxN3VkWGNEYUdDR0J4UzNtOkt1MkRNd3FkejM2Q042NXc5R1ZNcVViZ2ZMREhGZlVN'
  };

  var formData = {
    code: 'wERSrXFWhUNqgVTGdL',
    redirect_uri: 'http://localhost:4200/dashboard',
    grant_type: 'authorization_code'
  };

  // Step 1. Exchange authorization code for access token.
  request.post({ url: accessTokenUrl, form: formData, headers: headers, json: true }, function(err, response, body) {
    if (body.error) {
console.log('BIEN');
      return res.status(400).send({ message: body.error_description });
    }
console.log('BIEN');
    var params = {
      access_token: body.access_token
    };

    // Step 2. Retrieve information about the current user.
    request.get({ url: userApiUrl, qs: params, json: true }, function(err, response, profile) {

      // Step 2.5. Retrieve current user's email.
      request.get({ url: emailApiUrl, qs: params, json: true }, function(err, response, emails) {
        var email = emails.values[0].email;

        // Step 3a. Link user accounts.
        if (req.header('Authorization')) {
          User.findOne({ bitbucket: profile.uuid }, function(err, existingUser) {
            if (existingUser) {
              return res.status(409).send({ message: 'There is already a Bitbucket account that belongs to you' });
            }
            var token = req.header('Authorization').split(' ')[1];
            var payload = jwt.decode(token, config.TOKEN_SECRET);
            User.findById(payload.sub, function(err, user) {
              if (!user) {
                return res.status(400).send({ message: 'User not found' });
              }
              user.bitbucket = profile.uuid;
              user.email = user.email || email;
              user.picture = user.picture || profile.links.avatar.href;
              user.displayName = user.displayName || profile.display_name;
              user.save(function() {
                var token = createJWT(user);
                res.send({ token: token });
              });
            });
          });
        } else {
          // Step 3b. Create a new user account or return an existing one.
          User.findOne({ bitbucket: profile.id }, function(err, existingUser) {
            if (existingUser) {
              var token = createJWT(existingUser);
              return res.send({ token: token });
            }
            var user = new User();
            user.bitbucket = profile.uuid;
            user.email = email;
            user.picture = profile.links.avatar.href;
            user.displayName = profile.display_name;
            user.save(function() {
              var token = createJWT(user);
              res.send({ token: token });
            });
          });
        }
      });
    });
  });
});



//Starting server
app.listen(app.get('port'), ()=>{
    console.log('Servidor en puerto ',app.get('port'));
});